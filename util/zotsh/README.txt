ZotSH - v.0.0.3

Client for browsing RedDAVs.

Install
-------

ZotSH requires 'requests'(1).
Please refer to requests docs on how to install it (2)

Extract somewhere and launch zotsh.py


Description
-----------

Update: 2019-08-14

Have just looked at this after several years of bitrot and made some updates.
it functions for cli DAV access on your assigned hub, but magic-auth to dav repos on other hubs
(e.g. the host command) needs to be updated to work with openwebauth.

The ability to browse remote WebDAV repositories outside your own server is not as useful as it was once was; as the top level WebDAV folder on each site might have been hidden by the administrator for security/privacy reasons. On these sites the root directory will be empty. You can only proceed by

cd username

OR you can connect to the site using

host username@hostname

if you know a username on that site and if they have given you the requisite permission *or* their directory contains publicly readable content. 

----
ZotSH is a command line WebDAV client for Nomad platforms.
It knows how to magic-auth to remote hubs using OpenWebAuth.

ZotSH uses 'easywebdav' library (0) with small modifications
to 'zotify' it. (See easywebdav/LICENSE)



Commands
--------

host <hostname>
	Authenticate to 'hostname' and switch to it

host <username@hostname>
	Authenticate to 'hostname' and switch to it and automatically cd to the 'username' directory
	
cd <dirname|..>
	change remote dir

ls [path] [-a] [-l] [-d]
	list remote files in current dir if 'path' not defined
	-a list all, show hidden dot-files
	-l list verbose
	-d list only dirs

exists <path>
	Check existence of 'path'
	
mkdir <name>
	Create directory 'name'

mkdirs <path>
	Create parent directories to path, if they don't exist

rmdir <name>
	Delete directory 'name'

delete <path>
	Delete file 'path'

upload <local_path> [remote_path]
	Upload local file 'local_path' to 'remote_path'

download <remote_path> [local_path]
	Download remote file 'remote_path' and save it as 'local_path'

cat <remote_path>
	Print content of 'remote_path'

pwd
	Print current path

lcd
lpwd
lls
	Local file management (cd, pwd, and ls)

quit
help



Config
------

Create a .zotshrc file in your home or in same folder with zotsh.py:


	[zotsh]
	host = https://yourhost.com/
	username = your_username
	password = your_password


Optionally adds

        verify_ssl = false

to skip verification of ssl certs


Changelog
----------
0.0.2		Fix "CommandNotFound" exception, new 'cat' command

0.0.1		First release


Links
-----

_0 : https://github.com/amnong/easywebdav
_1 : http://docs.python-requests.org/en/latest/
_2 : http://docs.python-requests.org/en/latest/user/install/
